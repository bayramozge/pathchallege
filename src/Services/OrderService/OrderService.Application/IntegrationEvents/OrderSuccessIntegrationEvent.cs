﻿using EventBus.Base.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Application.IntegrationEvents
{
    public class OrderSuccessIntegrationEvent : IntegrationEvent
    {
        public Guid OrderId { get; }
        public string Username { get; set; }

        public OrderSuccessIntegrationEvent(Guid orderId, string username)
        {
            OrderId = orderId;
            Username = username;
        }
    }
}
