﻿using IdentityServer.Application.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Application.Services
{
    public class IdentityService : IIdentityService
    {
        private List<UserData> _users = new List<UserData>() {
            new UserData("bayramozge","1q2w3E*","bayramozge@yandex.com","Bayram","ÖZGE","5438324613") ,
            new UserData("testuser","123456","user1@yandex.com","Test","User","")
        };

        public Task<LoginResponseModel> Login(LoginRequestModel requestModel)
        {
            var checkUser = _users.Where(x => x.UserName == requestModel.UserName && x.Password == requestModel.Password).FirstOrDefault();

            if (checkUser == null)
                return Task.FromResult(new LoginResponseModel { IsLogin = false });

            var claims = new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, checkUser.UserName),
                new Claim(ClaimTypes.Name, checkUser.Name),
                new Claim(ClaimTypes.Surname, checkUser.Surname),
                new Claim(ClaimTypes.MobilePhone, checkUser.PhoneNumber),
                new Claim(ClaimTypes.Email, checkUser.Email),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddDays(1);
            var token = new JwtSecurityToken(claims: claims, expires: expiry, signingCredentials: creds, notBefore: DateTime.Now);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

            LoginResponseModel response = new()
            {
                IsLogin= true,
                UserToken = encodedJwt,
                UserName = requestModel.UserName
            };

            return Task.FromResult(response);
        }
    }

    public class UserData
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }

        public UserData(string userName, string password, string email, string name, string surname, string phonenumber)
        {
            UserName = userName;
            Password = password;
            Email = email;
            Name = name;
            Surname = surname;
            PhoneNumber = phonenumber;
        }
    }
}
