﻿using EventBus.Base.Events;

namespace NotificationService.IntegrationEvents.Events
{
    public class OrderSuccessIntegrationEvent : IntegrationEvent
    {
        public Guid OrderId { get; }
        public string Username { get; set; }

        public OrderSuccessIntegrationEvent(Guid orderId, string username)
        {
            OrderId = orderId;
            Username = username;
        }
    }
}
