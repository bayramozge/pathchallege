﻿using EventBus.Base.Abstraction;
using NotificationService.IntegrationEvents.Events;
using Serilog;

namespace NotificationService.IntegrationEvents.EventHandlers
{
    class OrderSuccessIntegrationEventHandler : IIntegrationEventHandler<OrderSuccessIntegrationEvent>
    {

        public Task Handle(OrderSuccessIntegrationEvent @event)
        {
          
            Log.Logger.Information($"Order Success with OrderId: {@event.OrderId},Username: {@event.Username}");
            Console.WriteLine($"Order Success with OrderId: {@event.OrderId},Username: {@event.Username}");

            return Task.CompletedTask;
        }
    }
}
