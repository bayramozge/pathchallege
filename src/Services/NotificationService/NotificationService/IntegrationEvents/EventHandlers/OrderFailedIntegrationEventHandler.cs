﻿using EventBus.Base.Abstraction;
using NotificationService.IntegrationEvents.Events;
using Serilog;

namespace NotificationService.IntegrationEvents.EventHandlers
{
    class OrderFailedIntegrationEventHandler : IIntegrationEventHandler<OrderFailedIntegrationEvent>
    {
        public Task Handle(OrderFailedIntegrationEvent @event)
        {
            // Send Fail Notification (Sms, EMail, Push)

            Log.Logger.Information($"Order  failed with OrderId: {@event.OrderId}, ErrorMessage: {@event.ErrorMessage}");
            Console.WriteLine($"Order  failed with OrderId: {@event.OrderId}, ErrorMessage: {@event.ErrorMessage}");
            return Task.CompletedTask;
        }
    }
}
